(function (doc) {

  doc.addEventListener('DOMContentLoaded', () => {
    const tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    const firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    doc.querySelector('.video-click')
        .addEventListener('click', function () {
          const code = this.dataset.code;
          const frameWrap = doc.querySelector('#video-view');
          this.classList.add('hide');
          new YT.Player(frameWrap, {
            height: '326',
            width: '561',
            videoId: code,
            events: {
              'onReady': function onPlayerReady(event) {
                event.target.playVideo();
              },
            }
          });
        });

  })

})(document)